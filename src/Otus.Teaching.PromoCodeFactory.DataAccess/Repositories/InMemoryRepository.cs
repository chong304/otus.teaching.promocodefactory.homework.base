﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task CreateAsync(T entity)
        {
            var dataList = Data.ToList();
            dataList.Add(entity);
            Data = dataList;
            await Task.CompletedTask;
        }

        public async Task UpdateAsync(T entity)
        {
            var dataList = Data.ToList();
            var index = dataList.FindIndex(x => x.Id == entity.Id);
            if (index >= 0)
            {
                dataList[index] = entity;
                Data = dataList;
            }
            await Task.CompletedTask;
        }

        public async Task DeleteAsync(T entity)
        {
            var dataList = Data.ToList();
            var index = dataList.FindIndex(x => x.Id == entity.Id);
            if (index >= 0)
            {
                dataList.RemoveAt(index);
                Data = dataList;
            }
            await Task.CompletedTask;
        }
    }
}